﻿
using TCRSStaticSiteGenerator.Cli.Features.Commands;
using McMaster.Extensions.CommandLineUtils;

namespace TCRSStaticSiteGenerator.Cli;

class Program
{
    /// <summary>
    /// Program entry, pass execution to our main command handler
    /// </summary>
    /// <param name="args">Args to pass to handler</param>
    /// <typeparam name="TssgCommand">Command handler</typeparam>
    /// <returns>Status Code 0 = success, 1 = failure</returns>
    public static Task<int> Main(string[] args) => CommandLineApplication.ExecuteAsync<TssgCommand>(args);
}
