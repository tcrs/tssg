namespace TCRSStaticSiteGenerator.Core.Features.Configuration;

/// <summary>
/// Main configuration settings, parsed from tssg.json file.
/// </summary>
public class TssgConfig
{
    public string SiteTitle { get; set; } = "My TCRS Static Site Generator Site";
    public string IndexFileName { get; set; } = "Index";
    public string LayoutFileName { get; set; } = "_Layout";
    public List<string> StaticExtensionIgnoreList { get; set; } = new List<string> { ".cshtml", ".md", ".DS_Store", ".json" };
    public bool ExcludeHtmlExtension { get; set; } = true;
    public bool EnableMarkdownExtensions { get; set; } = false;
}